package com.example.dingtoi

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_detail.*
import android.util.Log
import android.telephony.TelephonyManager
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import android.os.Build
import java.util.*
import android.os.StatFs
import android.os.Environment
import androidx.annotation.RequiresApi
import com.example.dingtoi.data.DeviceInfor
import android.provider.Settings
import java.math.BigInteger
import android.app.ActivityManager
import android.hardware.Camera


class DetailActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        this.getDeviceInfor("a")
        btnConfirm.setOnClickListener {
            // Handler code here.
            val intent = Intent(this@DetailActivity, ReCheckActivity::class.java)
            startActivity(intent)
        }
        val name = this.getCarrierNames();
        Log.d("Carrier name", name)
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun getDeviceInfor(transactionId: String) {
//        val versionCode = BuildConfig.VERSION_CODE
        val versionName = BuildConfig.VERSION_NAME
        val carrierNames =  this.getCarrierNames()
        val country = this.getResources().getConfiguration().locale.getCountry()
        val locale: String = Locale.getDefault().getLanguage()
        val model:String = android.os.Build.MODEL
        val freeDistStorage: Long = this.getFreeDiskStorage()
        val manufacturer = android.os.Build.MANUFACTURER
        val totalMemory: Long = this.getTotalMemory()
        val deviceName: String = this.getDeviceName()
        val storageSize = this.getTotalDiskCapacity()
        val maxMemory = this.getMaxMemory()
        val numberOfCamera = Camera.getNumberOfCameras()
        val deviceInfor: DeviceInfor = DeviceInfor(
            versionName,
            carrierNames,
            country ,
            locale,
            freeDistStorage,
            manufacturer,
            model ,
            totalMemory,
            deviceName,
            storageSize,
            maxMemory,
            numberOfCamera
        )
        this.showInfor(deviceInfor)
    }
    private fun showInfor(device: DeviceInfor){
        txtVersion.text = device.versionName
        txtCarrier.text = device.carrier
        txtDeviceCountry.text = device.deviceCountry
        txtDeviceLocale.text = device.deviceLocale
        txtFreeDiskStorage.text = device.freeDiskStorage.toString()
        txtModel.text = device.model
        txtTotalMemory.text = device.totalMemory.toString()
        txtManufacturer.text = device.manufacturer
        txtDeviceName.text = device.deviceName
        txtStorageSize.text = device.storageSize.toString()
        txtMaxMemory.text = device.maxMemory.toString()
        txtContent.text = device.numberOfCamera.toString()
    }

    @SuppressLint("MissingPermission")
    private fun getCarrierNames(): String{
        if (Build.VERSION.SDK_INT > 22) {
            //for dual sim mobile
            val localSubscriptionManager = SubscriptionManager.from(this)

            if (localSubscriptionManager.activeSubscriptionInfoCount > 1) {
                //if there are two sims in dual sim mobile
                val localList = localSubscriptionManager.activeSubscriptionInfoList
                val simInfo = localList[0] as SubscriptionInfo
                val simInfo1 = localList[1] as SubscriptionInfo

                val sim1 = simInfo.displayName.toString()
                val sim2 = simInfo1.displayName.toString()
                return "$sim1, $sim2"

            } else {
                //if there is 1 sim in dual sim mobile
                val tManager = baseContext
                    .getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

                val sim1 = tManager.networkOperatorName
                return "$sim1"

            }

        } else {
            //below android version 22
            val tManager = baseContext
                .getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

            val sim1 = tManager.networkOperatorName
            return "$sim1"
        }
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun getFreeDiskStorage2(): Long{
        val stat = StatFs(Environment.getExternalStorageDirectory().getPath())

        val bytesAvailable = stat.getBlockSizeLong().toLong() * stat.getBlockCountLong().toLong()
        val megAvailable = bytesAvailable / 1048576
        return megAvailable
    }

    /*private fun getTotalMemory(): Long{
        val statFs = StatFs(Environment.getRootDirectory().absolutePath)
        return (statFs.blockCount * statFs.blockSize).toLong()
    }*/
    private fun getFreeMemory(): Long{
        val statFs = StatFs(Environment.getRootDirectory().absolutePath)
        return (statFs.availableBlocks * statFs.blockSize).toLong()
    }



    private fun getFreeDiskStorage(): Long {
        try {
            val external = StatFs(Environment.getExternalStorageDirectory().absolutePath)
            val availableBlocks: Long
            val blockSize: Long

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
                availableBlocks = external.availableBlocks.toLong()
                blockSize = external.blockSize.toLong()
            } else {
                availableBlocks = external.availableBlocksLong
                blockSize = external.blockSizeLong
            }

            return BigInteger.valueOf(availableBlocks).multiply(BigInteger.valueOf(blockSize))
                .toLong()
        } catch (e: Exception) {
            return -1
        }

    }

    private fun getDeviceName(): String {
        try {
            val bluetoothName = Settings.Secure.getString(
                this.getContentResolver(),
                "bluetooth_name"
            )
            if (bluetoothName != null) {
                return bluetoothName
            }

            if (Build.VERSION.SDK_INT >= 25) {
                val deviceName = Settings.Global.getString(
                    this.getContentResolver(),
                    Settings.Global.DEVICE_NAME
                )
                if (deviceName != null) {
                    return deviceName
                }
            }
        } catch (e: Exception) {
            // same as default unknown return
        }

        return "unknown"
    }
    private fun getTotalDiskCapacity(): Long {
        try {
            val root = StatFs(Environment.getRootDirectory().absolutePath)
            return BigInteger.valueOf(root.blockCount.toLong())
                .multiply(BigInteger.valueOf(root.blockSize.toLong())).toLong()
        } catch (e: Exception) {
            return -1
        }

    }
    private fun getMaxMemory(): Long {
        return Runtime.getRuntime().maxMemory()
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    fun getTotalMemory(): Long {
        val actMgr =
            this.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val memInfo = ActivityManager.MemoryInfo()
        if (actMgr != null) {
            actMgr.getMemoryInfo(memInfo)
        } else {
            System.err.println("Unable to getMemoryInfo. ActivityManager was null")
            return -1
        }
        return memInfo.totalMem
    }
}
