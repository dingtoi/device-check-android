package com.example.dingtoi

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.ActivityManager
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Point
import android.hardware.Camera
import androidx.appcompat.app.AppCompatActivity
import android.provider.Settings
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dingtoi.adapter.DeviceInfoAdapter
import com.example.dingtoi.data.DeviceInfoRow
import kotlinx.android.synthetic.main.activity_device_info.*
import kotlin.collections.ArrayList
import android.hardware.fingerprint.FingerprintManager
import android.media.AudioManager
import android.net.ConnectivityManager
import android.os.*
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.WindowManager
import com.example.dingtoi.adapter.OnItemClickListener
import org.json.JSONObject
import java.io.*
import java.lang.Math.ceil


class DeviceInfoActivity : AppCompatActivity(), OnItemClickListener {
   /* val MY_PERMISSIONS_READ_PHONE_STATE = 0
    val MY_PERMISSIONS_CAMERA =1
    val PERMISSION_ALL = 10;*/

    val MOCK_DATA_FILE = "mock-data.json"
    val MOCK_DATA_DIR = "mydata"
    lateinit var deviceInfoAdapter:DeviceInfoAdapter

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_info)

        //draw view
        rvDeviceInfo.setHasFixedSize(true)
        rvDeviceInfo.layoutManager = LinearLayoutManager(this)
        /*val deviceInfo = this.getDeviceInfor()
        var deviceInfoAdapter = DeviceInfoAdapter(deviceInfo)
        rvDeviceInfo.adapter = deviceInfoAdapter*/
        this.showDeviceInfor()
        btnConfirm2.setOnClickListener {
            // Handler code here.
            val intent:Intent = Intent(this@DeviceInfoActivity, InstalledAppsActivity::class.java)
            startActivity(intent)
        }


    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun getDeviceInfor(): List<DeviceInfoRow> {
        val rs = ArrayList<DeviceInfoRow>()

        val usedTime = this.getUsedTime().toString()+" h" // mock data
        rs.add(DeviceInfoRow("Used Time", usedTime))

        val batteryRechargedTime = this.getBatteryRechargedTime().toString() + " times" // mock data
        rs.add(DeviceInfoRow("Battery Recharged Time", batteryRechargedTime))

        rs.add(DeviceInfoRow("Maximum Battery Capacity", "100%")) // mock data

        rs.add(DeviceInfoRow("Dead Pixels","0")) //mock data

        val cameraFunction = if (this.getNumberofCameras() > 0) "Yes" else "No"
        rs.add(DeviceInfoRow("Camera Function", cameraFunction))

        val isSupportedBluetooth = this.checkBluetoothSupported()
        rs.add(DeviceInfoRow("Support Bluetooth", isSupportedBluetooth))

        val isWifiSupported = this.checkWifiSupported()
        rs.add(DeviceInfoRow("Support Wifi", isWifiSupported))

        val freeDiskStorage = this.getFreeDiskStorage()
        rs.add(DeviceInfoRow("Free Disk Storage", "$freeDiskStorage GB"))

        rs.add(DeviceInfoRow("Activated","1 times")) // mock data

        val checkFingerprint =  if (this.checkHasFingterprinter()) "Yes" else "No"
        rs.add(DeviceInfoRow("Fingerprinter", checkFingerprint))

        rs.add(DeviceInfoRow("Receive incoming calls", "Yes"))// mock data
        rs.add(DeviceInfoRow("Send outgoing calls", "Yes")) // mock data
        rs.add(DeviceInfoRow("Send and receive text and data", "Yes")) // mock data

        val isFlashSupported = this.checkFlashSupported()
        rs.add(DeviceInfoRow("Support Flash", isFlashSupported))

        rs.add(DeviceInfoRow("Volume and Speaker control", "Yes"))// mock data
        rs.add(DeviceInfoRow("Screen Brightness control", "Yes"))// mock data

        val versionName = this.getAndroidVersion()
        rs.add(DeviceInfoRow("Android Version", versionName))

        val cpuInfo = this.getProcessorInfo()
        //Processor
        rs.add(DeviceInfoRow("CPU", cpuInfo))


        //=======================================// old Data
        /*
//        val versionCode = BuildConfig.VERSION_CODE
        val deviceType = this.getDeviceTypeFromPhysicalSize()
        rs.add(DeviceInfoRow("Device Type", deviceType))
        val carrierNames =  this.getCarrierNames()
        rs.add(DeviceInfoRow("Carrier", carrierNames))
        val country = this.getResources().getConfiguration().locale.getCountry()
        rs.add(DeviceInfoRow("Device Country", country))
        val locale: String = Locale.getDefault().getLanguage()
        rs.add(DeviceInfoRow("Device Locale", locale))
        val model:String = android.os.Build.MODEL
        rs.add(DeviceInfoRow("Device Name", model))
        val productName = Build.PRODUCT
//        rs.add(DeviceInfoRow("Product", productName))
//        rs.add(DeviceInfoRow("Hardware", Build.HARDWARE))
        val manufacturer = android.os.Build.MANUFACTURER
        rs.add(DeviceInfoRow("Manufacturer", manufacturer))
        val totalMemory = this.getTotalMemory()
        rs.add(DeviceInfoRow("Total Memory", totalMemory.toString() + " GB"))
        val freeMemory = this.getFreeMemory().toString()
        rs.add(DeviceInfoRow("Free Memory", freeMemory + " GB"))
        /*val deviceName: String = this.getDeviceName()
        rs.add(DeviceInfoRow("Device Name", deviceName))*/
        val freeDiskStorage = this.getFreeDiskStorage()
        rs.add(DeviceInfoRow("Free Disk Storage", freeDiskStorage.toString() + " GB"))
//        val maxMemory = this.getMaxMemory()
//        rs.add(DeviceInfoRow("Max Memory", maxMemory.toString()))
        /*val backCameraInfo = if(numberOfCamera > 0) this.getCameraInfo(0) else "Unkonwn"
        rs.add(DeviceInfoRow("Back Camera", backCameraInfo))

        val frontCameraInfo = if(numberOfCamera > 1) this.getCameraInfo(1)  else "Unkonwn"
        rs.add(DeviceInfoRow("Front Camera", frontCameraInfo))*/
        val dualSIM = this.checkDualSIM()
        rs.add(DeviceInfoRow("Dual SIM", dualSIM))
        //getBatteryCapacity
        /*val batteryCapacity = this.getBatteryCapacity(this)
        rs.add(DeviceInfoRow("Battery Capacity", batteryCapacity.toString()))*/
        val batteryCapacity = BatteryInfo().getBatteryCapacity(this).toString()
        rs.add(DeviceInfoRow("Battery Capacity", batteryCapacity + " mAh"))
        val batteryLevel = this.getBatteryLevel()
        rs.add(DeviceInfoRow("Battery Level", batteryLevel))
        val board = this.getBoard()
        rs.add(DeviceInfoRow("Board", board))
        val display = this.getDisplay()
        rs.add(DeviceInfoRow("Display", display))
        val cpuTemp = this.getCpuTemperature()
        rs.add(DeviceInfoRow("CPU Temperature", cpuTemp))
        val isRooted = this.isRootedDevice(this)
        rs.add(DeviceInfoRow("Rooted", isRooted.toString()))
        */
        //=======================================//
        return rs
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun showDeviceInfor() {
        val deviceInfo = this.getDeviceInfor()
         deviceInfoAdapter = DeviceInfoAdapter(deviceInfo)
        deviceInfoAdapter.setOnItemClickListener(this)


        rvDeviceInfo.adapter = deviceInfoAdapter
    }
    @SuppressLint("MissingPermission")
    private fun getCarrierNames(): String{
        var rs = DeviceInfoRow(null, null)
        if (Build.VERSION.SDK_INT > 22) {
            //for dual sim mobile
            val localSubscriptionManager = SubscriptionManager.from(this)

            if (localSubscriptionManager.activeSubscriptionInfoCount > 1) {
                //if there are two sims in dual sim mobile
                val localList = localSubscriptionManager.activeSubscriptionInfoList
                val simInfo = localList[0] as SubscriptionInfo
                val simInfo1 = localList[1] as SubscriptionInfo

                val sim1 = simInfo.displayName.toString()
                val sim2 = simInfo1.displayName.toString()
                return "$sim1, $sim2"

            } else {
                //if there is 1 sim in dual sim mobile
                val tManager = baseContext
                    .getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

                val sim1 = tManager.networkOperatorName
                return "$sim1"

            }

        } else {
            //below android version 22
            val tManager = baseContext
                .getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

            val sim1 = tManager.networkOperatorName
            return "$sim1"
        }
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun getFreeDiskStorage2(): Long{
        val stat = StatFs(Environment.getExternalStorageDirectory().getPath())

        val bytesAvailable = stat.getBlockSizeLong().toLong() * stat.getBlockCountLong().toLong()
        val megAvailable = bytesAvailable / 1048576
        return megAvailable
    }

    /*private fun getTotalMemory(): Long{
        val statFs = StatFs(Environment.getRootDirectory().absolutePath)
        return (statFs.blockCount * statFs.blockSize).toLong()
    }*/
    private fun getFreeMemory(): Float{
        val mi = ActivityManager.MemoryInfo()
        val activityManager =  getSystemService(ACTIVITY_SERVICE) as ActivityManager;
        activityManager.getMemoryInfo(mi)
        val availableMegs = mi.availMem / 0x100000L;
        return availableMegs/1000.toFloat()
    }



    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun getFreeExternalDiskStorage(): Long {
        try {
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            var path = Environment.getExternalStorageDirectory();
            var stat =  StatFs(path.getPath());
            var blockSize = stat.getBlockSizeLong();
            var availableBlocks = stat.getAvailableBlocksLong();
                 return (availableBlocks * blockSize);
            } else {
                return -1;
            }
        } catch (e: Exception) {
            return -1
        }

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun getTotalExternalMemorySize(): Long {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            var path = Environment.getExternalStorageDirectory();
            var stat =  StatFs(path.getPath());
            var blockSize = stat.getBlockSizeLong();
            var totalBlocks = stat.getBlockCountLong();
            return (totalBlocks * blockSize);
        } else {
            return -1;
        }
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun getFreeDiskStorage(): Double {
        try {
            /*val external = StatFs(Environment.getExternalStorageDirectory().absolutePath)
            val availableBlocks: Long
            val blockSize: Long

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
                availableBlocks = external.availableBlocks.toLong()
                blockSize = external.blockSize.toLong()
            } else {
                availableBlocks = external.availableBlocksLong
                blockSize = external.blockSizeLong
            }

            return BigInteger.valueOf(availableBlocks).multiply(BigInteger.valueOf(blockSize))
                .toLong()*/
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                var path = Environment.getDataDirectory()
                var stat =  StatFs(path.getPath())
                var blockSize = stat.getBlockSizeLong()
                var availableBlocks = stat.getAvailableBlocksLong()
                var tmp = (availableBlocks * blockSize)/(1024*1024*1024).toFloat();
                var rs =  Math.round(tmp * 1000.0) / 1000.0
                return rs
            } else {
                return -1.0;
            }
        } catch (e: Exception) {
            return -1.0;
        }

    }

    private fun getDeviceName(): String {
        try {
            val bluetoothName = Settings.Secure.getString(
                this.getContentResolver(),
                "bluetooth_name"
            )
            if (bluetoothName != null) {
                return bluetoothName
            }

            if (Build.VERSION.SDK_INT >= 25) {
                val deviceName = Settings.Global.getString(
                    this.getContentResolver(),
                    Settings.Global.DEVICE_NAME
                )
                if (deviceName != null) {
                    return deviceName
                }
            }
        } catch (e: Exception) {
            // same as default unknown return
        }

        return "unknown"
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private fun getTotalDiskCapacity(): Double {
       /* try {
            val root = StatFs(Environment.getRootDirectory().absolutePath)
            return BigInteger.valueOf(root.blockCount.toLong())
                .multiply(BigInteger.valueOf(root.blockSize.toLong())).toLong()
        } catch (e: Exception) {
            return -1
        }*/
        var path = Environment.getDataDirectory();
        var stat =  StatFs(path.getPath());
        var blockSize = stat.getBlockSizeLong();
        var totalBlocks = stat.getBlockCountLong();
        var tmp = (totalBlocks * blockSize)/(1024*1024*1024).toFloat()
        var rs =  Math.round(tmp * 1000.0) / 1000.0
        return rs

    }
    private fun getMaxemory(): Long {
        return Runtime.getRuntime().maxMemory()
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    fun getTotalMemory(): Double {
        val actMgr =
            this.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val memInfo = ActivityManager.MemoryInfo()
        if (actMgr != null) {
            actMgr.getMemoryInfo(memInfo)
        } else {
            System.err.println("Unable to getMemoryInfo. ActivityManager was null")
            return -1.toDouble()
        }
        var m =  memInfo.totalMem/(1024*1024*1024).toFloat()
        var rs =  Math.round(m * 1000.0) / 1000.0
        return rs
    }

    @SuppressLint("MissingPermission")
    private fun checkDualSIM(): String {
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            val sm = SubscriptionManager.from(this)
            if(sm.getActiveSubscriptionInfoCountMax() == 2){
                return "true"
            }else return "false"
        } else {
            return "Unknown"
        }
    }

    private fun getAndroidVersion(): String {
        val version = Build.VERSION.RELEASE.toString()
        return version
    }


    private fun getBatteryLevel(): String{
        val bm = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
            return batLevel.toString()
        } else {
            return "Unknown"
        }

    }


    @RequiresApi(Build.VERSION_CODES.M)
    private  fun checkHasFingterprinter(): Boolean {
        val fingerprintManager =
            this.getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager
        return fingerprintManager.isHardwareDetected
    }

    /*fun getBackCameraResolutionInMp(): Float {
        val noOfCameras = Camera.getNumberOfCameras()
        var maxResolution = -1f
        var pixelCount: Long = -1
        for (i in 0 until noOfCameras) {
            val cameraInfo = CameraInfo()
            Camera.getCameraInfo(i, cameraInfo)

            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                val camera = Camera.open(i)
                val cameraParams = camera.parameters
                for (j in 0 until cameraParams.supportedPictureSizes.size) {
                    val pixelCountTemp =
                        (cameraParams.supportedPictureSizes[j].width * cameraParams.supportedPictureSizes[j].height).toLong() // Just changed i to j in this loop
                    if (pixelCountTemp > pixelCount) {
                        pixelCount = pixelCountTemp
                        maxResolution = pixelCountTemp.toFloat() / 1024000.0f
                    }
                }

                camera.release()
            }
        }

        return maxResolution
    }*/

    private fun getDeviceTypeFromPhysicalSize(): String {
        // Find the current window manager, if none is found we can't measure the device physical size.
        val windowManager = this.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            ?: return "UNKNOWN"

        // Get display metrics to see if we can differentiate handsets and tablets.
        // NOTE: for API level 16 the metrics will exclude window decor.
        val metrics = DisplayMetrics()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            windowManager.defaultDisplay.getRealMetrics(metrics)
        } else {
            windowManager.defaultDisplay.getMetrics(metrics)
        }

        // Calculate physical size.
        val widthInches = metrics.widthPixels / metrics.xdpi.toDouble()
        val heightInches = metrics.heightPixels / metrics.ydpi.toDouble()
        val diagonalSizeInches = Math.sqrt(Math.pow(widthInches, 2.0) + Math.pow(heightInches, 2.0))

        return if (diagonalSizeInches >= 3.0 && diagonalSizeInches <= 6.9) {
            // Devices in a sane range for phones are considered to be Handsets.
            "HANDSET"
        } else if (diagonalSizeInches > 6.9 && diagonalSizeInches <= 18.0) {
            // Devices larger than handset and in a sane range for tablets are tablets.
            "TABLET"
        } else {
            // Otherwise, we don't know what device type we're on/
            "UNKNOWN"
        }
    }

    private fun getBoard(): String{
        val board = Build.BOARD
        return board
    }

    private fun getDisplay(): String {
        val display = getWindowManager().getDefaultDisplay();
        val size =  Point();
        display.getSize(size);
        val width = size.x;
        val height = size.y;

        val dm =  DisplayMetrics()
        getWindowManager().getDefaultDisplay().getMetrics(dm)
        val x = Math.pow(width.toDouble()/dm.xdpi, 2.0);
        val y = Math.pow(height.toDouble()/dm.ydpi, 2.0);
        val screenInches = Math.sqrt(x+y)
        val inch = Math.round(screenInches * 10.0) / 10.0
        return "$width x $height ($inch inch)"

    }


    private fun getProcessorInfo(): String{
//        Log.i("CPU2", "CPU_ABI : " + Runtime.getRuntime().availableProcessors());
        var processBuilder =  ProcessBuilder("/system/bin/cat", "/proc/cpuinfo")
        var  process = processBuilder.start()
        var  inputStream = process.getInputStream()
        var  byteArry =  ByteArray(1024)
        var str: String ="";
        var numberOfCores: String? = null
        var modelName: String? = null
        var cpuMHz: String? =  null
        var cacheSize: String? = null

        while(inputStream.read(byteArry) != -1){
            str =  str.plus(String(byteArry))
//            Log.d("CPU", str)
        }
        inputStream.close()
        var lines= str.lines()
        lines.forEach {

            var tmp = it.split(":").toTypedArray()
//            Log.d("CPU", tmp[0].toString() )
            when(tmp[0].replace("\\s".toRegex(), "")){
               /*"cpucores"->{
                  if(numberOfCores == null)
                      numberOfCores = tmp[1]
               }
                "cpuMHz"->{
                    if(cpuMHz == null)
                        cpuMHz = tmp[1]
                }
                "modelname"->{
                    if(modelName == null)
                        modelName=tmp[1]
                }
                "cachesize"->{
                    if(cacheSize == null)
                        cacheSize=tmp[1]
                }*/

                "CPUarchitecture"->{
                    if(numberOfCores == null)
                        numberOfCores = tmp[1]
                }
               /* "modelname"->{
                    if(modelName == null)
                        modelName=tmp[1]
                }*/
                "Hardware"->{
                    if(modelName == null)
                        modelName=tmp[1]
                }
            }

        }
        return "$numberOfCores x $modelName"

    }

    private  fun isRootedDevice( context: Context): Boolean {

        var rootedDevice = false;
        val buildTags = android.os.Build.TAGS;
        if (buildTags != null && buildTags.contains("test-keys")) {
            Log.e("Root Detected", "1");
            rootedDevice = true;
        }

        // check if /system/app/Superuser.apk is present
        try {
            val file =  File("/system/app/Superuser.apk");
            if (file.exists()) {
                Log.e("Root Detected", "2");
                rootedDevice = true;
            }
        } catch (e: Exception) {
            //Ignore
        }

        //check if SU command is executable or not
        try {
            Runtime.getRuntime().exec("su");
            Log.e("Root Detected", "3");
            rootedDevice = true;
        } catch ( e: Exception) {
            //Ignore
        }

        //check weather busy box application is installed
        val packageName = "stericson.busybox"; //Package for busy box app
        val pm = context.getPackageManager();
        try {
            Log.e("Root Detected", "4");
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            rootedDevice = true;
        } catch (e: Exception) {
            //App not installed
        }

        return rootedDevice;
    }

    private fun getCameraInfo(carmeraId: Int): String{
        var camera=Camera.open(carmeraId)
        var parameters = camera.getParameters();
        var sizes = parameters.getSupportedPictureSizes();
        var h =0;
        var w =0;
        var resolution: Double =0.toDouble()
        for ( size in sizes) {
            var tmpR  = ((size.width.toDouble() * size.height.toDouble())/1024000)
            if(tmpR > resolution){
                h = size.height
                w = size.width
                resolution = ceil(tmpR)
            }
        }
        camera.release();
        return "$resolution MP ($w x $h)"
    }

    private fun getNumberofCameras(): Int{
            return  Camera.getNumberOfCameras()
    }

    private fun getCpuTemperature(): String
    {

        return try {
            var process = Runtime.getRuntime().exec("cat sys/class/thermal/thermal_zone0/temp")
            process.waitFor();
            val reader = BufferedReader(InputStreamReader(process.inputStream))
            val line = reader.readLine()
//            Log.d("Temp", line.toString())
            if(line!=null) {
                val temp = line.toFloat()
                val c = temp / 1000.0f
                val f = (c*9/5)+32
                return "$c \u2103 / $f \u2109"
            }else{
                "Unknown"
            }
        } catch ( e: Exception) {
            Log.d("Temp error", e.toString())
            e.printStackTrace()
            "Unknown"
        }
    }

    private fun checkBluetoothSupported(): String {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if(mBluetoothAdapter == null){
            return "No"
        }
        return "Yes"
    }

    private fun checkWifiSupported(): String {
        val connManager =  getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI) ?: return "No"
        return "Yes"
    }

    private fun checkFlashSupported(): String{
        val hasFlash = this.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
        if(hasFlash)
            return "Yes"
        return "No"
        this.volumeControlStream = AudioManager.STREAM_MUSIC;

    }

    private fun checkAudioControl(): String {
        val audioManager = applicationContext.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val x = Settings.System.SCREEN_BRIGHTNESS
        Log.d("xxx", x)

        if(audioManager == null) return "No"
        return "Yes"

    }

    private fun checkScreenBrightness(): String {
        try {
            val curBrightnessValue= Settings.System.getInt(
                contentResolver, Settings.System.SCREEN_BRIGHTNESS)
            val x = (100*curBrightnessValue)/255
            return if(x in 0..100)
                "Yes"
            else
                "No"

            return x.toString()
            } catch ( e : Settings.SettingNotFoundException) {
                // TODO Auto-generated catch block
            return "No"
//                e.printStackTrace();
            }

    }



    private fun writeFileToInternalStorage(data: String) {
    try {
        var outputStreamWriter =  OutputStreamWriter(this.openFileOutput(MOCK_DATA_FILE, Context.MODE_PRIVATE))
        outputStreamWriter.write(data)
        outputStreamWriter.close()
    }
    catch ( e: IOException) {
        Log.e("Write File", "File write failed: $e");
    }
    }

    private fun readFileFromInternalStorage(): String{
        var fileContent = ""
        try {
            var inputStream: InputStream = this.openFileInput(MOCK_DATA_FILE);

            if ( inputStream != null ) {
                var inputStreamReader = InputStreamReader(inputStream)
                var bufferedReader = BufferedReader(inputStreamReader)
                var receiveString :String? = null
                var stringBuilder =  StringBuilder()

                /*while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }*/
                while ({ receiveString = bufferedReader.readLine(); receiveString }() != null) {
                    stringBuilder.append("\n").append(receiveString);
                }
                inputStream.close()
                fileContent = stringBuilder.toString()
            }
        }
        catch ( e: FileNotFoundException) {
            Log.e("Read file", "File not found: $e");
        } catch ( e: IOException) {
            Log.e("Read file", "Can not read file: $e");
        }

        return fileContent
    }

    /*
    Mock data used time
    */
    private fun getUsedTime(): Int {
        var usedTime =  (100..999).random()
        var mockData = this.readFileFromInternalStorage()
        if(mockData == "" || !JSONObject(mockData).has("usedTime")){
            if(mockData ==""){
                var json= JSONObject()
                json.put("usedTime", usedTime)
                this.writeFileToInternalStorage(json.toString())
            }else {
                var json = JSONObject(mockData)
                json.put("usedTime", usedTime)
                this.writeFileToInternalStorage(json.toString())
            }
        }else {
            usedTime = JSONObject(mockData).getInt("usedTime")
        }
        return usedTime
    }

    /*
    * Mock data battery recharged time
    * */
    private fun getBatteryRechargedTime(): Int {
        var batteryRechargedTime =  (100..999).random()
        var mockData = this.readFileFromInternalStorage()
        if(mockData == "" || !JSONObject(mockData).has("batteryRechargedTime")){
            var json = JSONObject(mockData)
            json.put("batteryRechargedTime", batteryRechargedTime)
            this.writeFileToInternalStorage(json.toString())
        }else {
            batteryRechargedTime = JSONObject(mockData).getInt("batteryRechargedTime")
        }
        return batteryRechargedTime
    }

    override fun onItemClick(view: View?, position: Int) {
        Log.d("cc", "cc")
        var data: DeviceInfoRow = deviceInfoAdapter.getData().get(position);
        val intent:Intent = Intent(this@DeviceInfoActivity, PaintActivity::class.java)
        startActivity(intent)
    }
}
