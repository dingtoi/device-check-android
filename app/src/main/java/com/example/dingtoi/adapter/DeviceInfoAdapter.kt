package com.example.dingtoi.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.dingtoi.DeviceInfoActivity
import com.example.dingtoi.R
import com.example.dingtoi.data.DeviceInfoRow

interface OnItemClickListener {
    fun onItemClick(view: View?, position: Int)
}

interface ClickListener {
    fun onPositionClicked(position: Int)
    fun onLongClicked(position: Int)
}
class DeviceInfoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var content: TextView? = view.findViewById(R.id.tvContent)
    var title: TextView? = view.findViewById(R.id.tvTitle)
    var btnCheckDeadPixel: Button? = view.findViewById(R.id.btnCheckDeadPixel)
}
class DeviceInfoAdapter(private var infoList : List<DeviceInfoRow>) : RecyclerView.Adapter<DeviceInfoViewHolder>() {

    lateinit var listener: OnItemClickListener
    lateinit var  mOnItemClickLitener: OnItemClickListener;
    var iku = infoList;

    open fun setOnItemClickListener(mOnItemClickLitener: DeviceInfoActivity) {
        this.mOnItemClickLitener = mOnItemClickLitener
    }

    fun getData(): List<DeviceInfoRow> {
        return this.iku
    }

    override fun getItemCount(): Int {
        return if (infoList.isNotEmpty()) infoList.size else 1
    }

    override fun onBindViewHolder(holder: DeviceInfoViewHolder, position: Int) {

        if(position != this.itemCount -1) {
            if (infoList.isEmpty()) {
                holder.title?.setText(R.string.unknow)
                holder.content?.setText(R.string.unknow)
            } else {
                val infor = infoList[position]
                holder.title!!.text = infor.title
                holder.content!!.text = infor.content
            }
        }else {
            holder.btnCheckDeadPixel!!.setOnClickListener{
                mOnItemClickLitener.onItemClick(holder.itemView, position)
            }
        }
    }
    override fun getItemViewType( position: Int): Int{
        return if(position != this.itemCount -1)
            0
        else
            1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceInfoViewHolder {
        if(viewType == 0) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.device_info_item_row, parent, false)
            return DeviceInfoViewHolder(view)
        } else{
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.draw_row, parent, false)
            return DeviceInfoViewHolder(view)
        }
    }
}