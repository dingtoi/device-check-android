package com.example.dingtoi.adapter

import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.dingtoi.R
import com.example.dingtoi.data.InstalledAppsRow

class InstalledAppsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var appName: TextView = view.findViewById(R.id.tvAppName)
    var appIcon: ImageView = view.findViewById(R.id.imgAppIcon)
    var totalTime: TextView = view.findViewById(R.id.tvTotalTime)
}
class InstalledAppsAdapter(private var appList : List<InstalledAppsRow>) : RecyclerView.Adapter<InstalledAppsViewHolder>() {
    override fun getItemCount(): Int {
        return if (appList.isNotEmpty()) appList.size else 1
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: InstalledAppsViewHolder, position: Int) {


        if (appList.isEmpty()) {
            holder.appName.setText(R.string.unknow)
//            holder.appIcon.setImageDrawable()
        }else {
            val appInfo = appList[position]
            holder.appName.text = appInfo.appName
            holder.appIcon.setImageDrawable(appInfo.appIcon)
            var totalTime = appInfo.usageStats?.totalTimeInForeground
            if(totalTime !== null){

                val s = totalTime % 60;
                val m = (totalTime / 60) % 60;
                val h = (totalTime / (60 * 60)) % 24;
                holder.totalTime.text = "$h:$m:$s"
            } else {
                holder.totalTime.text ="Unknown"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InstalledAppsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.installed_apps_item_row, parent, false)

        return InstalledAppsViewHolder(view)
    }
}