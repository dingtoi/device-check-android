package com.example.dingtoi

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.example.dingtoi.viewModels.TestCameraViewModel
import kotlinx.android.synthetic.main.activity_test_camera.*
import androidx.lifecycle.Observer


class TestCameraActivity : AppCompatActivity() {

    val REQUEST_BACK_CAMERA = 1
    val REQUEST_FRONT_CAMERA = 2
    val MY_PERMISSIONS_CAMERA =1
    var isFrontCamera = true
    private val testCameraViewModel: TestCameraViewModel by lazy { ViewModelProviders.of(this).get(TestCameraViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_camera)

        // test back camera
        btnTestBackCamera.setOnClickListener {
            isFrontCamera = false
            if (ContextCompat.checkSelfPermission(this@TestCameraActivity, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
                // Permission is not granted
                ActivityCompat.requestPermissions(this@TestCameraActivity,
                    arrayOf(Manifest.permission.CAMERA),
                    MY_PERMISSIONS_CAMERA)
            } else {
                this.takePictureWithBackCamera()
            }

        }

        btnTestFrontCamera.setOnClickListener {
            isFrontCamera = true
            if (ContextCompat.checkSelfPermission(this@TestCameraActivity, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
                // Permission is not granted
                ActivityCompat.requestPermissions(this@TestCameraActivity,
                    arrayOf(Manifest.permission.CAMERA),
                    MY_PERMISSIONS_CAMERA)
            } else {
               this.takePictureWithFrontCamera()
            }

        }
        btnConfirm3.setOnClickListener {
            val intent = Intent(this@TestCameraActivity, CheckAudioActivity::class.java)
            startActivity(intent)

        }
        loadImageLiveData()


    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_CAMERA -> {
                // If request is cancelled, the result arrays are empty.
                if (ContextCompat.checkSelfPermission(this@TestCameraActivity, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_DENIED) {

                    if(isFrontCamera){
                       this.takePictureWithFrontCamera()
                    }else{
                        this.takePictureWithBackCamera()
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun takePictureWithBackCamera(){
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUEST_BACK_CAMERA)
    }
    private fun takePictureWithFrontCamera(){
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra("android.intent.extras.CAMERA_FACING", REQUEST_FRONT_CAMERA)
        startActivityForResult(intent, REQUEST_FRONT_CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("CAMERA", data?.extras.toString())
        if (requestCode == REQUEST_BACK_CAMERA && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
//            imgFromBackCamera.setImageBitmap(imageBitmap)
            testCameraViewModel.setBackImage(imageBitmap)

        } else if(requestCode == REQUEST_FRONT_CAMERA && resultCode == RESULT_OK){
            val imageBitmap = data?.extras?.get("data") as Bitmap
//            imgFromFrontCamera.setImageBitmap(imageBitmap)
            testCameraViewModel.setFrontImage(imageBitmap)
        }

    }

    private fun loadImageLiveData(){
        testCameraViewModel.getFrontImage().observe(this, Observer<Bitmap> {
           imageBitmap-> imgFromFrontCamera.setImageBitmap(imageBitmap)
        })

        testCameraViewModel.getBackImage().observe(this, Observer<Bitmap> {
                imageBitmap-> imgFromBackCamera.setImageBitmap(imageBitmap)
        })
    }
}
