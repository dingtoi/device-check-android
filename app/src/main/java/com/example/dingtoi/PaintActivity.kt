package com.example.dingtoi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.dingtoi.paint.PaintView
import android.util.DisplayMetrics
import android.view.Menu
import kotlinx.android.synthetic.main.activity_paint.*


class PaintActivity : AppCompatActivity() {
    private var paintView: PaintView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paint)
        paintView = findViewById<PaintView>(R.id.paintView)
        var metrics = DisplayMetrics()
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        paintView?.init(metrics)
        paintView?.normal()
        btnConfirmPaint.setOnClickListener {
            // Handler code here.
            val intent = Intent(this@PaintActivity, InstalledAppsActivity::class.java)
            startActivity(intent)
        }

    }


}
