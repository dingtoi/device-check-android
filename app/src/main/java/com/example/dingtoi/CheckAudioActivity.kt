package com.example.dingtoi

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Bundle
import android.util.Log
import android.view.View.OnClickListener
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.skyfishjy.library.RippleBackground
import kotlinx.android.synthetic.main.activity_check_audio.*
import java.io.IOException
import android.media.MediaMetadataRetriever
import android.net.Uri
import com.example.dingtoi.timer.CountDownTimer
import com.example.dingtoi.timer.CountUpTimer
import java.lang.Exception


private const val LOG_TAG = "AudioRecordTest"
private const val REQUEST_RECORD_AUDIO_PERMISSION = 200

class CheckAudioActivity : AppCompatActivity() {


    private var fileName: String = ""

    private var recordButton: RecordButton? = null
    private var recorder: MediaRecorder? = null

    private var playButton: PlayButton? = null
    private var player: MediaPlayer? = null

    // Requesting permission to RECORD_AUDIO
    private var permissionToRecordAccepted = false
    private var permissions: Array<String> = arrayOf(Manifest.permission.RECORD_AUDIO)

    private lateinit var upTimer: CountUpTimer
    private lateinit var downTimer: CountDownTimer
    private lateinit var rippleBackground : RippleBackground
    private var mStartRecording = true
    private var mStartPlaying = true

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionToRecordAccepted = if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        } else {
            false
        }
        if (!permissionToRecordAccepted) finish()
    }

    private fun onRecord(start: Boolean) = if (start) {
        startRecording()
    } else {
        stopRecording()
    }

    private fun onPlay(start: Boolean) = if (start) {
        startPlaying()
    } else {
        stopPlaying()
    }

    private fun startPlaying() {
        player = MediaPlayer().apply {
            try {
                setDataSource(fileName)
                prepare()
                start()
            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed")
            }
        }
    }

    private fun stopPlaying() {
        player?.release()
        player = null
    }

    private fun startRecording() {
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setOutputFile(fileName)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

            try {
                prepare()
            } catch (e: IOException) {
                Log.e(LOG_TAG, "prepare() failed")
            }

            start()
        }
    }

    private fun stopRecording() {
        recorder?.apply {
            stop()
            release()
        }
        recorder = null
    }

    internal inner class RecordButton(ctx: Context) : Button(ctx) {

        var mStartRecording = true

        var clicker: OnClickListener = OnClickListener {
            onRecord(mStartRecording)
            text = when (mStartRecording) {
                true -> "Stop recording"
                false -> "Start recording"
            }
            mStartRecording = !mStartRecording
        }

        init {
            text = "Start recording"
            setOnClickListener(clicker)
        }
    }

    internal inner class PlayButton(ctx: Context) : Button(ctx) {
        var mStartPlaying = true
        var clicker: OnClickListener = OnClickListener {
            onPlay(mStartPlaying)
            text = when (mStartPlaying) {
                true -> "Stop playing"
                false -> "Start playing"
            }
            mStartPlaying = !mStartPlaying
        }

        init {
            text = "Start playing"
            setOnClickListener(clicker)
        }
    }

    override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        setContentView(R.layout.activity_check_audio)

        // Record to the external cache directory for visibility
        fileName = "${externalCacheDir?.absolutePath}/audiorecordtest.3gp"
        rippleBackground= animationContent as RippleBackground
        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION)

        //recording

        btnRecord.setOnClickListener{

            when (mStartRecording) {
                true ->  startRecord()
                false -> stopRecord()
            }
//            mStartRecording = !mStartRecording

        }

        //playing

         btnPlay.setOnClickListener{
             when(mStartPlaying){
                 true -> startPlay()
                 false -> stopPlay()
             }
//             mStartPlaying = !mStartPlaying

         }

        btnConfirmAudio.setOnClickListener {
            // Handler code here.
            val intent = Intent(this@CheckAudioActivity, PaintActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStop() {
        super.onStop()
        recorder?.release()
        recorder = null
        player?.release()
        player = null
    }

    @SuppressLint("ResourceAsColor")
    fun makeButtonDisable(button: Button) {
        button.isEnabled = false
        button.isClickable = false
//        button?.setTextColor(ContextCompat.getColor(textView.context, R.color.mainTextColor))
//        button?.setBackgroundColor(R.color.gray)
    }

    @SuppressLint("ResourceAsColor")
    fun makeButtonEnable(button: Button){
        button.isEnabled = true
        button.isClickable = true
    }

    private fun startPlay(){
        try {
            onPlay(mStartPlaying)
            val uri = Uri.parse(fileName)
            val mmr = MediaMetadataRetriever()
            mmr.setDataSource(this, uri)
            val durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
            val millSecond = Integer.parseInt(durationStr)
            downTimer = object : CountDownTimer(millSecond.toLong()) {
                override fun onTick(second: Int) {
                    var h = second / 60
                    var s = second - (h * 60)
                    txtHours.text = if (h <= 9) "0$h" else h.toString()
                    txtSeconds.text = if (s <= 9) "0$s" else s.toString()
//                if(second == 0) stopRecord()
                }

                override fun onFinish() {
                    stopPlay()
                }
            }
            downTimer.start()
            rippleBackground.startRippleAnimation()
            btnPlay.text = "Stop playing"
            makeButtonDisable(btnRecord)
            mStartPlaying = !mStartPlaying
            Log.d("StartPlay", "Start play")
        }catch (e: Exception){
            Log.d("Playing Error", e.toString())
        }
    }

    private fun stopPlay(){
        onPlay(mStartPlaying)
        rippleBackground.stopRippleAnimation()
        btnPlay.text = "Start playing"
        makeButtonEnable(btnRecord)
        if(::downTimer.isInitialized)
            downTimer.cancel()
        txtHours.text ="00"
        txtSeconds.text="00"
        mStartPlaying = !mStartPlaying
    }

    private fun startRecord(){
        onRecord(mStartRecording)
         upTimer = object : CountUpTimer(30000) {
            override fun onTick(second: Int) {
                var h = second /60
                var s = second - (h*60)
                txtHours.text = if(h<= 9) "0$h" else h.toString()
                txtSeconds.text = if(s<= 9) "0$s" else s.toString()
//                if(second == 0) stopRecord()
            }

             override fun onFinish() {
                 super.onFinish()
                 stopRecord()
             }
        }
        upTimer.start()
        rippleBackground.startRippleAnimation()
        btnRecord.text = "Stop recording"
        makeButtonDisable(btnPlay)
        mStartRecording = !mStartRecording

    }

    private fun stopRecord(){
        onRecord(mStartRecording)
        upTimer.cancel()
        rippleBackground.stopRippleAnimation()
        btnRecord.text = "Start recording"
        makeButtonEnable(btnPlay)
        mStartRecording = !mStartRecording
//        Log.d("StopRecord", "Stopped")
    }
}