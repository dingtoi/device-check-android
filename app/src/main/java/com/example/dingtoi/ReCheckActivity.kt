package com.example.dingtoi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_re_check.*

class ReCheckActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_re_check)
        btnReCheck.setOnClickListener {
            // Handler code here.
            val intent = Intent(this@ReCheckActivity, InputTransactionActivity::class.java)
            startActivity(intent)
        }
    }
}
