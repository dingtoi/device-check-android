package com.example.dingtoi

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_input_transaction.*
import android.os.Build
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import kotlin.properties.Delegates

//import android.provider.Settings.Secure.getString

class InputTransactionActivity : AppCompatActivity() {
    private lateinit var progress: ProgressDialog

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_transaction)
        // click button to open new activity
        btnCheck.setOnClickListener {
            // Handler code here.
            // validate transaction id
            var transactionId =  etTransaction.text.toString()
            if(transactionId?.isNullOrBlank()){
                Toast.makeText(this, "transaction id is required!",
                    Toast.LENGTH_LONG).show()
            }else {
                this.showProgressbar()
                val checkTransaction = CheckTransaction(this)
                checkTransaction?.execute(transactionId)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        this.hideProgressbar()
    }

    private fun showProgressbar(){

        progress = ProgressDialog(this)
        progress.setTitle("Please Wait!!");
        progress.setMessage("Wait!!");
        progress.setCancelable(false);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
        window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    private fun hideProgressbar(){
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        progress.setCancelable(false);
        this.progress.dismiss()
    }


    companion object {
        private class CheckTransaction(context: Context) : AsyncTask<String, Void, String>() {
            var propContext : Context by Delegates.notNull()
            init {
                propContext = context
            }
            //        val okHttpClient: OkHttpClient = OkHttpClient.Builder().retryOnConnectionFailure(true).build()
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun doInBackground(vararg url: String?): String {
                //
                val transactionId = url[0].toString()
                val okHttpClient: OkHttpClient =
                    OkHttpClient.Builder().retryOnConnectionFailure(true).build()
                val apiURL: String = BuildConfig.API_URL + "/transaction/check/" + transactionId
                val builder = Request.Builder()
                builder.url(apiURL)
                val request = builder.build()

                val response = okHttpClient.newCall(request).execute()
                return response.body?.string().toString()
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                val jsonResult =  JSONObject(result)
                val rs: Boolean = jsonResult.getBoolean("isSuccess")
                if(true){
//                    val intent = Intent(propContext, TestActivity::class.java)
                    val intent = Intent(propContext, DeviceInfoActivity::class.java)
                    propContext.startActivity(intent)

                }else{
                    Toast.makeText(propContext, "Transaction not found",
                        Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}
