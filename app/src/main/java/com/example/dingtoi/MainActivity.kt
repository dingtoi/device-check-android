package com.example.dingtoi

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.* // dung de call cac component tu xml, ko can thong qua getById
import android.app.AppOpsManager
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T




class MainActivity : AppCompatActivity() {
    val MY_PERMISSIONS_READ_PHONE_STATE = 0
    val MY_PERMISSIONS_CAMERA =1
    val PERMISSION_ALL = 10;

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.requestPermissions()
        // click button to open new activity
        btnNext.setOnClickListener {
            // Handler code here.
            val intent = Intent(this@MainActivity, InputTransactionActivity::class.java)
            startActivity(intent)
        }
        val appOps = getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
        val mode = appOps.checkOpNoThrow(
            AppOpsManager.OPSTR_GET_USAGE_STATS,
            android.os.Process.myUid(), packageName
        )
        if(mode !== AppOpsManager.MODE_ALLOWED)
            startActivity(Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS))
    }

    fun hasPermissions(context: Context, vararg permissions: String): Boolean = permissions.all {
        ActivityCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
    }
    private fun requestPermissions() {
        /*if (ContextCompat.checkSelfPermission(this@DeviceInfoActivity, Manifest.permission.READ_PHONE_STATE)
            != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            ActivityCompat.requestPermissions(this@DeviceInfoActivity,
                arrayOf(Manifest.permission.READ_PHONE_STATE),
                MY_PERMISSIONS_READ_PHONE_STATE)
        }
        if (ContextCompat.checkSelfPermission(
                this@DeviceInfoActivity,
                Manifest.permission.CAMERA
            )
            == PackageManager.PERMISSION_DENIED
        ) {
            // Permission is not granted
            ActivityCompat.requestPermissions(
                this@DeviceInfoActivity,
                arrayOf(Manifest.permission.CAMERA),
                MY_PERMISSIONS_CAMERA
            )
        }*/


        val PERMISSIONS = arrayOf(
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CAMERA
        )
        if (!hasPermissions(this, Manifest.permission.READ_PHONE_STATE,  Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_READ_PHONE_STATE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    //skip
                } else {
                    this.closeNow()
                }
                return
            }
            MY_PERMISSIONS_CAMERA -> {
                // If request is cancelled, the result arrays are empty.
                if (ContextCompat.checkSelfPermission(this@MainActivity, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_DENIED) {

                    //skip
                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                    this.closeNow()
                }
                return
            }
            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun  closeNow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            finishAffinity()
        } else {
            finish()
        }
    }
}
