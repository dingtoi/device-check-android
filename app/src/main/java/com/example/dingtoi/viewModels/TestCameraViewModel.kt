package com.example.dingtoi.viewModels

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TestCameraViewModel : ViewModel() {
    private val backImage: MutableLiveData<Bitmap> = MutableLiveData<Bitmap>()
    private val frontImage:MutableLiveData<Bitmap> = MutableLiveData<Bitmap>()

    fun getBackImage(): LiveData<Bitmap> {
        return backImage as LiveData<Bitmap>
    }

    fun getFrontImage(): LiveData<Bitmap> {
        return frontImage
    }

    fun setBackImage(image: Bitmap){
        backImage.setValue(image)
    }

    fun setFrontImage(image: Bitmap){
        frontImage.setValue(image)
    }
}