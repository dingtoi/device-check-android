package com.example.dingtoi.viewModels

import android.graphics.Bitmap
import android.media.MediaRecorder
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CheckAudioViewModel : ViewModel() {
    // private var recorder: MediaRecorder? = null
    private var recorder: MutableLiveData<MediaRecorder>? = MutableLiveData()
//    private val frontImage:MutableLiveData<Bitmap> = MutableLiveData<Bitmap>()

    fun getRecorder(): MutableLiveData<MediaRecorder>? {
        return recorder
    }



    fun setRecorder(r: MediaRecorder){
        recorder?.value = r
    }


}