package com.example.dingtoi

import android.app.ProgressDialog
import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.dingtoi.data.InstalledAppsRow
import android.content.pm.PackageInfo
import android.content.pm.ApplicationInfo
import android.content.pm.ResolveInfo
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dingtoi.adapter.InstalledAppsAdapter
import kotlinx.android.synthetic.main.activity_installed_apps.*
import java.util.*
import kotlin.collections.ArrayList
import android.view.View
import android.os.AsyncTask
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlin.properties.Delegates


class InstalledAppsActivity : AppCompatActivity() {
    lateinit var mUsageStatsManager: UsageStatsManager
    private lateinit var progress: ProgressDialog

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_installed_apps)
        mUsageStatsManager = this
            .getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager //Context.
        //draw view
        rvInstalledApps.setHasFixedSize(true)
        rvInstalledApps.layoutManager = LinearLayoutManager(this)

        val wd = window
        val  ia= InstalledApps(this, tvNumberOfApps, rvInstalledApps, wd)
            ia?.execute()
//        pgLoadingInstalledAppsPage.visibility = View.GONE
        btnInstalledAppsConfirm.setOnClickListener{
            val intent = Intent(this@InstalledAppsActivity, ReCheckActivity::class.java)
            startActivity(intent)
        }
    }

    private fun showProgressbar(){

        progress = ProgressDialog(this)
        progress.setTitle("Please Wait!!");
        progress.setMessage("Wait!!");
        progress.setCancelable(true);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
    }
    private fun hideProgressbar(){
        this.progress.dismiss()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun getInstalledApps(): List<InstalledAppsRow>{
        var appList = ArrayList<InstalledAppsRow>()
        var usageStatistics = this.getUsageStatistics()
        val packs = packageManager.getInstalledPackages(0)
        /*
        * adasd*/
        val mainIntent = Intent(Intent.ACTION_MAIN, null)
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)

        val pkgAppsList = this.packageManager
            .queryIntentActivities(mainIntent, 0)
        for (i in packs.indices) {
            val p = packs[i]
            if (isSystemPackage(p) === false) {
                val appName = p.applicationInfo.loadLabel(packageManager).toString()
                val icon = p.applicationInfo.loadIcon(packageManager)

                val usageStats = this.getUsageStats(appName, usageStatistics,pkgAppsList)
                appList.add(InstalledAppsRow(appName, icon, usageStats))
            }
        }
        return appList
    }
    private fun isSystemPackage(pkgInfo: PackageInfo): Boolean {
        return pkgInfo.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM != 0
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun  showInstalledApps(){
            val appList = this.getInstalledApps()
            tvNumberOfApps.text = appList.count().toString()
            val installedAppsAdapter = InstalledAppsAdapter(appList)
            rvInstalledApps.adapter = installedAppsAdapter
//            pgLoadingInstalledAppsPage.visibility = View.GONE
//

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun getUsageStatistics(): List<UsageStats> {
        // Get the app statistics since one year ago from the current time.
        val cal = Calendar.getInstance()
        cal.add(Calendar.YEAR, -1)

        var stats = mUsageStatsManager
            .queryUsageStats(
                UsageStatsManager.INTERVAL_YEARLY, cal.timeInMillis,
                System.currentTimeMillis()
            )

        return stats
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun getUsageStats (appNameInput: String, usageStatsList: List<UsageStats>, pkgAppsList: MutableList<ResolveInfo>): UsageStats? {



        for (usageStats in usageStatsList) {

//            var usageStats = usageStatsList[i]
//            val appName = this.packageManager
//                .getApplicationIcon(usageStats.packageName);
            val appName = this.getAppNameFromPackage(usageStats.packageName, pkgAppsList)
            if(appNameInput === appName) return usageStats
        }
        return null
    }

    private fun getAppNameFromPackage(packageName: String, pkgAppsList: MutableList<ResolveInfo>): String? {


        for (app in pkgAppsList) {
            if (app.activityInfo.packageName == packageName) {
                return app.activityInfo.loadLabel(this.packageManager).toString()
            }
        }
        return null
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    internal fun getUsageStatistics2() {
        val cal = Calendar.getInstance()
        cal.add(Calendar.YEAR, -1)

        var usageStatsList = mUsageStatsManager
            .queryUsageStats(
                UsageStatsManager.INTERVAL_BEST, cal.timeInMillis,
                System.currentTimeMillis()
            )

        val appList = ArrayList<InstalledAppsRow>()
        for (i in usageStatsList.indices) {
            var usageStats = usageStatsList[i]

                val appIcon = packageManager
                    .getApplicationIcon(usageStats.packageName)
                val appName = usageStats.packageName
            appList.add(InstalledAppsRow(appName, appIcon, usageStats))
        }
    }

    companion object {
        lateinit var mUsageStatsManager: UsageStatsManager

        private lateinit var progress: ProgressDialog
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        private class InstalledApps(context: Context, tvNumberOfApps: TextView, rvInstalledApps: RecyclerView, wd: Window) : AsyncTask<String, Void, List<InstalledAppsRow>>() {


            var propContext : Context by Delegates.notNull()
            var tvNumberOfApps =tvNumberOfApps
            var rvInstalledApps =rvInstalledApps
            var wd = wd
            init {
                propContext = context
                mUsageStatsManager = propContext
                    .getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager //Context.
                showProgressbar()
            }
            private fun showProgressbar(){

                progress = ProgressDialog(propContext)
                progress.setTitle("Please Wait!!");
                progress.setMessage("Wait!!");
                progress.setCancelable(false);
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.show();
                wd.setFlags(
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
            private fun hideProgressbar(){

                wd.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                progress.setCancelable(true);
                progress.dismiss()
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            fun getInstalledApps(): List<InstalledAppsRow>{
                var appList = ArrayList<InstalledAppsRow>()
                var usageStatistics = this.getUsageStatistics()
                val packs = propContext.packageManager.getInstalledPackages(0)
                /*
                * adasd*/
                val mainIntent = Intent(Intent.ACTION_MAIN, null)
                mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)

                val pkgAppsList = propContext.packageManager
                    .queryIntentActivities(mainIntent, 0)
                for (i in packs.indices) {
                    val p = packs[i]
                    if (isSystemPackage(p) === false) {
                        val appName = p.applicationInfo.loadLabel(propContext.packageManager).toString()
                        val icon = p.applicationInfo.loadIcon(propContext.packageManager)

                        val usageStats = this.getUsageStats(appName, usageStatistics,pkgAppsList)
                        appList.add(InstalledAppsRow(appName, icon, usageStats))
                    }
                }
                return appList
            }
            private fun isSystemPackage(pkgInfo: PackageInfo): Boolean {
                return pkgInfo.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM != 0
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            private fun  showInstalledApps(){
                val appList = this.getInstalledApps()
                tvNumberOfApps.text = appList.count().toString()
                val installedAppsAdapter = InstalledAppsAdapter(appList)
                rvInstalledApps.adapter = installedAppsAdapter
//

            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            fun getUsageStatistics(): List<UsageStats> {
                // Get the app statistics since one year ago from the current time.
                val cal = Calendar.getInstance()
                cal.add(Calendar.YEAR, -1)

                var stats = mUsageStatsManager
                    .queryUsageStats(
                        UsageStatsManager.INTERVAL_YEARLY, cal.timeInMillis,
                        System.currentTimeMillis()
                    )

                return stats
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            fun getUsageStats (appNameInput: String, usageStatsList: List<UsageStats>, pkgAppsList: MutableList<ResolveInfo>): UsageStats? {



                for (usageStats in usageStatsList) {

//            var usageStats = usageStatsList[i]
//            val appName = this.packageManager
//                .getApplicationIcon(usageStats.packageName);
                    val appName = this.getAppNameFromPackage(usageStats.packageName, pkgAppsList)
                    if(appNameInput === appName) return usageStats
                }
                return null
            }

            private fun getAppNameFromPackage(packageName: String, pkgAppsList: MutableList<ResolveInfo>): String? {


                for (app in pkgAppsList) {
                    if (app.activityInfo.packageName == packageName) {
                        return app.activityInfo.loadLabel(propContext.packageManager).toString()
                    }
                }
                return null
            }
            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun doInBackground(vararg params: String?): List<InstalledAppsRow> {
                return getInstalledApps()
            }

            override fun onPostExecute(appList: List<InstalledAppsRow>?) {
                super.onPostExecute(appList)
                tvNumberOfApps.text = appList!!.count().toString()
                val installedAppsAdapter = InstalledAppsAdapter(appList)
                rvInstalledApps.adapter = installedAppsAdapter
                hideProgressbar()
            }
        }
    }


}

