package com.example.dingtoi.data

data class DeviceInfor(
    val versionName: String? ="unknown",
    val carrier: String? ="unknown",
    val deviceCountry: String? ="unknown",
//    val deviceId: String? ="undefined",
    val deviceLocale: String? ="unknown",
//    val deviceName: String? ="undefined",
    val freeDiskStorage: Long = -1,
    val manufacturer: String?="unknown",
    val model: String?="unknown",
//    val storageSize: String?="undefined",
    val totalMemory: Long= -1,
    val deviceName: String? ="unknown",
    val storageSize: Long = -1,
    val maxMemory: Long= -1,
    val numberOfCamera: Int
)