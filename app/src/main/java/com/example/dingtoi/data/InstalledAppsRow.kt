package com.example.dingtoi.data

import android.app.usage.UsageStats
import android.graphics.drawable.Drawable

data class InstalledAppsRow(val appName: String, val appIcon: Drawable,
                            var usageStats: UsageStats?)