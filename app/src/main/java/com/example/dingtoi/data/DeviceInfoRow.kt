package com.example.dingtoi.data

data class DeviceInfoRow (val title: String?, val content: String? = "Unknow")