package com.example.dingtoi.data

data class CPUInfor (
    val numberOfCores: Int,
    val modelName: String,
    val cupMHz: Float,
    val cacheSize: String
)